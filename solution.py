#!/usr/bin/python

import sys

START_ORDER = ord("a")

def enc_word(word, key):
    word_encrypted = ""
    for letter in word:
        word_encrypted += chr((((ord(letter) - START_ORDER) + key) % 26) + START_ORDER)

    return word_encrypted

def dec_word(word, key):
    word_decrypted = ""
    for letter in word:
        word_decrypted += chr((((ord(letter) - START_ORDER) - key) % 26) + START_ORDER)

    return word_decrypted

def enc_string(string, key):
    words = string.split(" ")

    words_encrypted = []
    for word in words:
        words_encrypted.append(enc_word(word, key))

    return " ".join(words_encrypted)

def dec_string(string, key):
    words = string.split(" ")

    words_decrypted = []
    for word in words:
        words_decrypted.append(dec_word(word, key))

    return " ".join(words_decrypted)

if __name__ == "__main__":
    enc_dec, string, key = sys.argv[1], sys.argv[2].lower(), int(sys.argv[3]) % 26

    if enc_dec == "enc":
        string_encrypted = enc_string(string, key)
        print(string_encrypted)
    elif enc_dec == "dec":
        string_decrypted = dec_string(string, key)
        print(string_decrypted)
    else:
        print("Please verify what you entered in the console \"" + enc_dec + "\" seems incorrect.")
