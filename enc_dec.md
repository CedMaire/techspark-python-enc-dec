# Secret Message
Today we are going to write a Python program that we can use to encrypt and decrypt messages. Encrypting a message is about making it unreadable to someone else. Decrypting an encrypted message is about getting back the original message so that we are able to read it. To achieve this we are going to use letter shifting (it's one of the simplest ways to encrypt a message), for example if we define an encryption key of 3, then the letter are changed in the following way: `a -> d, b -> e, c -> f, ..., w -> z, x -> a, y -> b, z -> c`. Here is an example:
```
>> python enc_dec.py enc "hello my name is cedric" 3
khoor pb qdph lv fhgulf
```
```
>> python enc_dec.py dec "khoor pb qdph lv fhgulf" 3
hello my name is cedric
```
As you can see the encrypted message is unreadable, but once decrypted with the same key used for encryption we get back the original message. Let's try to decrypt the message with a different key:
```
>> python enc_dec.py dec "khoor pb qdph lv fhgulf" 5
fcjjm kw lykc gq acbpga
```
The message is still unreadable, only the correct key will allow us to read the message.

## Setting Up the Code
1) Let's go back on the site: [onlinegdb.com/online_python_compiler](https://www.onlinegdb.com/online_python_compiler)
2) Then go to: [pastebin.com/raw/96LwGXJu](https://pastebin.com/raw/96LwGXJu)
3) Copy paste the code from PasteBin into the OnlineGDB Python Compiler

## Main Part of the Program
Let's look for this part of the  code, it's a the end.
```python
if __name__ == "__main__":
    enc_dec, string, key = sys.argv[1], sys.argv[2].lower(),
        int(sys.argv[3]) % 26

    if ???:
        ???
    elif ???:
        ???
    else:
        print("Please verify what you entered in the console \""
            + enc_dec + "\" seems incorrect.")
```
It's not mandatory to understand everything written here, simply get the main idea. This code is the main part of the program, it gets all the parameters the user entered in the console and decides wether it should encrypt or decrypt a message. `enc_dec` is a variable containing one of these two strings: `{"enc", "dec"}`. `string` contains the message the user wants to encrypt or decrypt. `key` contains an integer between 0 and 25, it's the key used to encrypt or decrypt a message.
Your job is to modify this part of the program so that if `enc_dec` is equal to the string `"enc"` it encrypts the message contained in `string` and if `enc_dec` is equal to the string `"dec"` it decrypts the message contained in `string`. To encrypt a message you can use the function `enc_string(a, b)` where `a` is the message you want to encrypt and `b` is the encryption key. To decrypt a message you can use the function `dec_string(a, b)` where `a` is the message you want to decrypt and `b` is the decryption key.

## Encryption & Decryption of a String
Let's now fill in the functions we need to encrypt and decrypt a string. Let's search for these two functions:
```python
def enc_string(string, key):
    words = ???.split(???)

    words_encrypted = ???
    for ??? in ???:
        ???.append(???)

    return ???.join(???)

def dec_string(string, key):
    words = ???.split(???)

    words_decrypted = ???
    for ??? in ???:
        ???.append(???)

    return ???.join(???)
```
As you can see you have quite a few places to fix. Each `???` has to be fixed. We will guide you through `enc_string` and then you have to implement `dec_string` on your own.
The first step for `enc_string` is to split the message into words. A string is composed of words and spaces (to make it simpler let's assume there is no punctuation). We want to split the whole sentence into all the words it is composed of. To do this we can use the function `split` that is used like this: `a.split(b)`, where `a` is the sentence (string) you want to split and `b` is the splitting character (here we want to split by spaces: `" "`). Once we got all the words of the sentence, let's create a new empty list to store the encryption of all these words, we can create an empty list like that: `[]`. We already created the variable `words_encrypted` for this. The third step is to go through the list of words, encrypt each one of these words and add them to the list `words_encrypted`. One can go through a list like this:
```python
for element in list:
    print(element)
```
You can use a different word than `element` to represent what this element is, for us it would be quite natural to use `word` instead of `element`. Then instead of `list` put our list of words. Inside of this for loop we want to encrypt the word and add it to our list of encrypted words. You can add an element to a list by writing: `a.append(b)`, where `a` is a list and `b` the element to add. To encrypt a words simply use: `enc_word(a, b)` where `a` is the word to encrypt and `b` the encryption key. The last step is to recreate the original sentence by linking all the encrypted words with a space. One can do this by writing: `a.join(b)` where `a` is the linking character (for us it is the space: `" "`) and `b` a list of words.
When you are done try to write the code for `dec_string(string, key)`.

## Encryption & Decryption of a Word
Let's now fill in the functions we need to encrypt and decrypt a word. Let's search for these two functions:
```python
def enc_word(word, key):
    word_encrypted = ???
    for ??? in ???:
        word_encrypted += chr((((ord(???) - START_ORDER) + ???)
            % 26) + START_ORDER)

    return ???

def dec_word(word, key):
    word_decrypted = ???
    for ??? in ???:
        word_decrypted += chr((((ord(???) - START_ORDER) - ???)
            % 26) + START_ORDER)

    return ???
```
Again, you have to fill in many parts of the code. As before we'll guide you through the function `enc_word(word, key)` and you then fill the function `dec_word(word, key)` on your own.
The first step is to create a new empty string/word. One can do this by writing: `""`, store it in the variable `word_encrypted`. Now we need to go through each letter of the word and change it into the encrypted version of this letter (remember the explanation in the first paragraph of this project). To go through each letter one can use the same syntax as before:
```python
for element in list:
    print(element)
```
But this time each element is a letter and instead of a list we have a string (which can be thought of as a list of characters!). Once you are able to get every letter of the word you can encrypt this letter and add it to `word_encrypted`. The encryption algorithm is already written for you (you don't need to understand this line of code since it uses parts of Python you didn't learn yet). But you still need to fill in two words. The `???` inside of the `ord(???)` function have to be replaced with the letter you want to encrypt. The `???` after the addition (` + ???`) have to be replaced by the encryption key. At the end return the encrypted word.
When you are done try to write the code for `dec_word(word, key)`.

## Testing
To test your program you have to give some parameters to your Python script. You can do this by writing inside of the command line arguments box just under the code box. The first argument you need to give is `enc` or `dec` depending on the fact that you want to encrypt or decrypt a message. The second argument (you need to put it inside of quotes if you want to encrypt more than one word) is the sentence you want to encrypt or decrypt. And the third argument is the encryption/decryption key. Here is an example to encrypt the sentence `hello my name is cedric`: `enc "hello my name is cedric" 3`. And an example to decrypt the result: `dec "khoor pb qdph lv fhgulf" 3`.
